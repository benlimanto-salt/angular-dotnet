using angular_net.Database;
using angular_net.Models;
using angular_net.Repositories.Interface;
using BCrypt.Net;
using Microsoft.EntityFrameworkCore;

namespace angular_net.Repositories.Infrastructure;

public class UserRepository : IUserRepository
{
    private DbContext _db;
    public UserRepository(AppDbContext db)
    {
        _db = db;
    }

    public User AddUser(User user)
    {
        return _db.Set<User>()
            .Add(user)
            .Entity;
    }

    public void DeleteUser(User user)
    {
        _db.Set<User>()
            .Where(q => q.Id == user.Id)
            .ExecuteDelete();
    }

    public Task<User?> GetUser(string username)
    {
        return _db.Set<User>()
            .Where(q => q.Username == username)
            .FirstOrDefaultAsync();
    }

    public Task<List<User>> GetUserList()
    {
        var dbQ =  _db.Set<User>().AsQueryable();

        return dbQ.ToListAsync();
    }

    public void SaveChanges()
    {
        _db.SaveChanges();
    }

    public User UpdateUser(User user)
    {
        return _db.Set<User>()
            .Update(user).Entity;
    }

    public async Task<bool> Login(string username, string password)
    {
        bool status = false;
        var user = await GetUser(username);
        
        if (user != null) 
        {
            string passHash = user.Password;
            status = BC.Verify(password, passHash);
        }

        return status;
    }
}