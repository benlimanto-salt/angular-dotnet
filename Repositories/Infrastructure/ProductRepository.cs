using angular_net.Database;
using angular_net.Models;
using Microsoft.EntityFrameworkCore;

namespace angular_net.Repositories;

public class ProductRepository : IProductRepository
{
    private DbContext _db;

    public ProductRepository(AppDbContext db)
    {
        _db = db;
    }

    public Product AddProduct(Product product)
    {
        return _db.Set<Product>()
            .Add(product)
            .Entity;
    }

    public void DeleteProduct(Product product)
    {
        _db.Set<Product>()
            .Where(q => q.Id == product.Id)
            .ExecuteDelete();
    }

    public Task<Product?> GetProduct(int id)
    {
        return _db.Set<Product>()
            .FirstOrDefaultAsync(q => q.Id == id);
    }

    public Task<List<Product>> GetProductList()
    {
        return _db.Set<Product>()
            .ToListAsync();
    }

    public void SaveChanges()
    {
        _db.SaveChanges();
    }

    public Product UpdateProduct(Product product)
    {
        return _db.Set<Product>()
            .Update(product)
            .Entity;
    }
}