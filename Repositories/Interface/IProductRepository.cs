using angular_net.Models;

namespace angular_net.Repositories;

public interface IProductRepository
{
    public Product AddProduct(Product product);
    public Product UpdateProduct(Product product);
    public void DeleteProduct(Product product);
    public Task<Product?> GetProduct(int id);
    public Task<List<Product>> GetProductList();
    public void SaveChanges();
}