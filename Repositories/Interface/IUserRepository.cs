using angular_net.Models;

namespace angular_net.Repositories.Interface;

public interface IUserRepository
{
    public User AddUser(User user);
    public User UpdateUser(User user);
    public void DeleteUser(User user);
    public Task<User?> GetUser(string username);
    public Task<List<User>> GetUserList();
    public void SaveChanges();
    public Task<bool> Login(string username, string password);
}