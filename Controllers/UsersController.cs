using angular_net.Models;
using angular_net.Models.Requests;
using angular_net.Models.Responses;
using angular_net.Repositories.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace angular_net.Controllers;

[ApiController]
[Route("/api/[controller]")]
[Authorize(Policy = "RequireAdminRole")]
public class UsersController : Controller
{
    private IUserRepository _userRepository;

    public UsersController(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    [HttpGet]
    public async Task<IActionResult> List()
    {
        var data = await _userRepository.GetUserList();

        return Ok(new ApiResponse() {
            Data = data
        });
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Individual([FromRoute] string id)
    {
        var data = await _userRepository.GetUser(id);

        if (data == null) 
            return NotFound(new ApiResponse() {
                Status = 404,
                Message = "Not found, no such user"
            });

        return Ok(new ApiResponse() {
            Data = data
        });
    }

    [HttpPost]
    public IActionResult InsertUser([FromBody] UserRequest user)
    {
        string hash = BC.HashPassword(user.Password);
        user.Password = hash;

        _userRepository.AddUser(user);
        _userRepository.SaveChanges();

        return Ok(new ApiResponse() {
            Message = "Success add new user"
        });
    }

    [HttpPatch("{id}")]
    public async Task<IActionResult> IndividualUpdate([FromRoute] string id, [FromBody] UserRequest user)
    {
        var data = await _userRepository.GetUser(id);
        
        if (data == null) 
            return NotFound(new ApiResponse() {
                Status = 404,
                Message = "Not found, no such user"
            });
        
        // Map Object Propeties Value
        data.Email = user.Email;
        data.FullName = user.FullName;

        if (user.Password != null && user.Password != "")
            data.Password = BC.HashPassword(user.Password);

        _userRepository.UpdateUser(data);
        _userRepository.SaveChanges();

        return Ok(new ApiResponse() {
            Message = "Success update user"
        });
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> IndividualDelete([FromRoute] string id)
    {
        var data = await _userRepository.GetUser(id);

        if (data == null) 
            return NotFound(new ApiResponse() {
                Status = 404,
                Message = "Not found, no such user"
            });

        _userRepository.DeleteUser(data);
        _userRepository.SaveChanges();

        return Ok(new ApiResponse() {
            Message = "Success delete user " + data.Username
        });
    }
}