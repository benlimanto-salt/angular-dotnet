using angular_net.Database;
using angular_net.Models;
using angular_net.Models.Responses;
using angular_net.Repositories;
using angular_net.Repositories.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace angular_net.Controllers;

[ApiController]
[Route("/api/[controller]")]
[Authorize(Policy = "ProductRole")]
public class ProductController : Controller
{
    private IProductRepository _productRepo;
    public ProductController(IProductRepository productRepository)
    {
        _productRepo = productRepository;
    }

    [HttpGet]
    public async Task<IActionResult> GetProductList()
    {
        var data  = await _productRepo.GetProductList();

        return Ok(new ApiResponse() {
            Data = data
        });
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetProduct([FromRoute] int id)
    {
        var data = await _productRepo.GetProduct(id);
        
        if (data == null)
            return NotFound(new ApiResponse() {
                Status = 404,
                Message = "No Product found"
            });
        
        return Ok(new ApiResponse() {
            Data = data
        });
    }
    
    [HttpDelete("{id}")]
    [Authorize(Policy = "RequireAdminRole")]
    public async Task<IActionResult> DeleteProduct([FromRoute] int id)
    {
        var data = await _productRepo.GetProduct(id);
        
        if (data == null)
            return NotFound(new ApiResponse() {
                Status = 404,
                Message = "No Product found"
            });

        _productRepo.DeleteProduct(data);
        _productRepo.SaveChanges();
        
        return Ok(new ApiResponse() {
            Message = "Success delete product"
        });
    }

    [HttpPost]
    public IActionResult InsertProduct([FromBody] Product product)
    {
        _productRepo.AddProduct(product);
        _productRepo.SaveChanges();
        
        return Ok(new ApiResponse() {
            Message = "Success add product"
        });
    }
    

    [HttpPatch("{id}")]
    public async Task<IActionResult> UpdateProductAsync([FromRoute] int id, [FromBody] Product product)
    {
        var data = await _productRepo.GetProduct(id);
        
        if (data == null)
            return NotFound(new ApiResponse() {
                Status = 404,
                Message = "No Product found"
            });
        
        data.Name = product.Name;
        data.Notes = product.Notes;
        data.Pricing = data.Pricing;

        var res = _productRepo.UpdateProduct(data);

        _productRepo.SaveChanges();

        return Ok(new ApiResponse());
    }


}