using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using angular_net.Models.Requests;
using angular_net.Models.Responses;
using angular_net.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using angular_net.Repositories.Interface;
using System.Net;

namespace angular_net.Controllers;

[ApiController]
[Route("auth")]
public class AuthenticationController : Controller 
{
    private readonly IConfiguration _conf;
    private IUserRepository _userRepo;

    public AuthenticationController(IConfiguration configuration, IUserRepository userRepository)
    {
        this._conf = configuration;
        _userRepo = userRepository;
    }

    [HttpPost("login")]
    public async Task<IActionResult> AuthAction([FromBody] LoginRequest request)
    {

        bool successLogin = await _userRepo.Login(request.Username, request.Password);

        if(!successLogin)
            return StatusCode((int) HttpStatusCode.Forbidden, new ApiResponse() 
            {
                Message = "User or password either wrong or not found"
            });

        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_conf.GetValue<string>("Authentication:Schemes:Bearer:IssuerSigningKey") ?? ""));
        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        var jwtHeader = new JwtHeader(creds);

        var audiences = _conf.GetSection("Authentication:Schemes:Bearer:ValidAudiences").Get<string[]>();
        var issuer = _conf.GetValue<string>("Authentication:Schemes:Bearer:ValidIssuer") ?? "";

        var config = _conf.GetSection("Authentication");
        var exp = DateTime.UtcNow.AddDays(1).ToEpoch().ToString();
        var claims = new[]
                {
                    new Claim("unique_name", request.Username),
                    new Claim(JwtRegisteredClaimNames.Sub, request.Username),
                    new Claim(JwtRegisteredClaimNames.Jti, request.Username),
                    new Claim("scope", "RequireAdminRole"),
                    new Claim("role", "admin"),
                    new Claim(JwtRegisteredClaimNames.Nbf, DateTime.UtcNow.ToEpoch().ToString(), ClaimValueTypes.Integer),
                    new Claim(JwtRegisteredClaimNames.Exp, exp, ClaimValueTypes.Integer),
                    new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToEpoch().ToString(), ClaimValueTypes.Integer),
                    new Claim(JwtRegisteredClaimNames.Iss, issuer)
                };

        var jwtBody = new JwtPayload(claims);
        
        jwtBody.Add("aud", audiences);

        // var token = new JwtSecurityToken(_conf.GetValue<string>("Authentication:Schemes:Bearer:ValidIssuer"),
        // jsonAud,
        // claims,
        // expires: DateTime.Now.AddMinutes(1),
        // signingCredentials: creds);

        var token = new JwtSecurityToken(jwtHeader, jwtBody);
        
        return Ok(new ApiResponse(){
            Data = new {token = new JwtSecurityTokenHandler().WriteToken(token), expired = exp+"000"} // This is fuck tup, but hemmm
        });
    }
}