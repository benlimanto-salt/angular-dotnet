using angular_net.Database;
using angular_net.Models;
using angular_net.Models.Responses;
using angular_net.Repositories.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace angular_net.Controllers;

[ApiController]
[Route("/api/[controller]")]
[Authorize(Policy = "RequireAdminRole")]
public class TasksController : Controller
{   
    private IUserRepository _userRepository;

    public TasksController(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    [HttpGet]
    public IActionResult GetTasks()
    {
        return Ok(new ApiResponse(){
            Data = new List<string>() {"a", "b", "c", "d"}
        });
    }

    [HttpGet("data")]
    [AllowAnonymous]
    public async Task<IActionResult> GetUser()
    {
        var data = await _userRepository.GetUserList();

        return Ok(new ApiResponse() {
            Data = data
        });
    }
}