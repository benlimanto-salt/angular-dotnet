using System.Text;
using angular_net.Authentication.Roles;
using angular_net.Helper;
using Microsoft.IdentityModel.Tokens;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();
builder.Services.AddAuthentication()
    .AddJwtBearer("Bearer", o => {
        o.SaveToken = false;
        var secret = builder.Configuration.GetValue<string>("Authentication:Schemes:Bearer:IssuerSigningKey") ?? "";
        
        o.TokenValidationParameters
            .IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));
        // Console.WriteLine(o.TokenValidationParameters.IssuerSigningKey.ToString());
    });

builder.Services.AddAuthorization()
    // Clear use of roles
    .AddAuthorizationRoles();

// Database Config PostgreSQL
builder.Services.AddPostgreDBContext();
// Transient Mapper for Repository
builder.Services.AddTransientMapper();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();
app.UseStatusCodePages("application/json","{{\"status\":{0}, \"message\": \"Oops, are you sure resource/end point really here? Or probably bad requests?\" }}");

app.UseCors(x => x
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html");

app.Run();
