using angular_net.Database;
using Microsoft.EntityFrameworkCore;

namespace angular_net.Helper;

public static class PostgreConnection
{
    public static void AddPostgreDBContext(this IServiceCollection services)
    {
        IConfiguration? configuration = services.BuildServiceProvider().GetService<IConfiguration>();
        var conString = (configuration?.GetValue<string>("PostgreConnectionString")) ?? throw new Exception("No Connection String Provided");

        services.AddDbContext<AppDbContext>(o => o
            .UseNpgsql(conString)
            .UseSnakeCaseNamingConvention());
    }
}