using angular_net.Repositories;
using angular_net.Repositories.Infrastructure;
using angular_net.Repositories.Interface;

namespace angular_net.Helper;

public static class TransientMapper
{
    public static void AddTransientMapper(this IServiceCollection services)
    {
        services.AddTransient<IUserRepository, UserRepository>();
        services.AddTransient<IProductRepository, ProductRepository>();
    }
}