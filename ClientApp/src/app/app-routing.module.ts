import { NgModule } from '@angular/core';
import { RouterModule, Routes, provideRouter, withComponentInputBinding } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { authGuard } from './middleware/auth.guard';
// import { DetailTaskListComponent } from './tasks/detail-task-list/detail-task-list.component';
// import { TaskListComponent } from './task-list/task-list.component';

const routes: Routes = 
[
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'counter', component: CounterComponent },
    { path: 'fetch-data', component: FetchDataComponent },
    {
        path: 'login', loadChildren: () => import("./pages/login-page/login-page.module")
            .then(m => m.LoginPageModule)
    },
    {
        path: 'master/users', loadChildren: () => import("./pages/master-user-page/master-user-page.module")
            .then(m => m.MasterUserPageModule),
        canActivate: [authGuard]
    },
    {
      path: 'master/product', loadChildren: () => import("./pages/master-product-page/master-product-page.module")
        .then(m => m.MasterProductPageModule),
      canActivate: [authGuard]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})],
  exports: [RouterModule],
  providers: [
    provideRouter(routes, withComponentInputBinding()),
  ]
})
export class AppRoutingModule { }
