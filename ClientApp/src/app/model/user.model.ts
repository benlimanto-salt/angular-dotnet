export interface User 
{
    id: string;
    fullName: string;
    password: string;
    username: string;
    email: string;
}