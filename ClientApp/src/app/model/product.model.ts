export interface Product {
    id: number;
    name: string;
    pricing: number;
    notes: string;
}
