import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

interface FormLoginModel 
{
  username: FormControl<string>;
  password: FormControl<string>;
}

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  public formLogin: FormGroup<FormLoginModel> = new FormGroup<FormLoginModel>({
    username: new FormControl('', {nonNullable: true, validators: [Validators.required]}),
    password: new FormControl('', {nonNullable: true, validators: [Validators.required]})
  });

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  public Submit()
  {
    this.authService.Login(this.username?.value, this.password?.value)
      .subscribe(d  => {
        console.log(d);
        this.authService.SetSession(d.data.token, d.data.expired);
        this.router.navigateByUrl("/master/users");
      });
  }

  get username() { return this.formLogin.get("username") }

  get password() { return this.formLogin.get("password") }

}
