import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MasterUserPageComponent } from './master-user-page.component';
import { UserInsertComponent } from './user-insert/user-insert.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserUpdateComponent } from './user-update/user-update.component';

const routes: Routes = [
    {   
        path: '', 
        component: MasterUserPageComponent, 
        children: [
            { path: '', component: UserListComponent },
            { path: 'add', component: UserInsertComponent },
            { path: ':id', component: UserUpdateComponent }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [],
    declarations: [],
    providers: [],
})
export class MasterUserPageRoutingModule { }
