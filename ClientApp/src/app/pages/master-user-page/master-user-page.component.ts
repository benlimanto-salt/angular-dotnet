import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-master-user-page',
  templateUrl: './master-user-page.component.html',
  styleUrls: ['./master-user-page.component.css']
})
export class MasterUserPageComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.GetTask()
      .subscribe(d => {
        console.log(d);
      });
  }

}
