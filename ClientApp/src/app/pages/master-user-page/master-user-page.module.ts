import { NgModule } from '@angular/core';

import { MasterUserPageComponent } from './master-user-page.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UserInsertComponent } from './user-insert/user-insert.component';
import { MasterUserPageRoutingModule } from './master-user-page-routing.module';
import { UserListComponent } from './user-list/user-list.component';
import { UserUpdateComponent } from './user-update/user-update.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MasterUserPageRoutingModule,
        RouterModule
    ],
    exports: [],
    declarations: [MasterUserPageComponent, UserInsertComponent, UserListComponent, UserUpdateComponent],
    providers: [],
})
export class MasterUserPageModule { }
