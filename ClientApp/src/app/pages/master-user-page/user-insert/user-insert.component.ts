import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/model/user.model';
import { UserService } from 'src/app/services/user.service';

interface FormUserInsertModel
{
  username: FormControl<string>;
  password: FormControl<string>;
  fullName: FormControl<string>;
  email: FormControl<string>;
}

@Component({
  selector: 'app-user-insert',
  templateUrl: './user-insert.component.html',
  styleUrls: ['./user-insert.component.css']
})
export class UserInsertComponent {
  public formField: string[] = ['username', 'password', 'fullName', 'email'];

  public formInsert: FormGroup<FormUserInsertModel> = new FormGroup<FormUserInsertModel>({
    username: new FormControl('', {nonNullable: true, validators: Validators.required}),
    password: new FormControl('', {nonNullable: true, validators: Validators.required}),
    fullName: new FormControl('', {nonNullable: true, validators: Validators.required}),
    email: new FormControl('', {nonNullable: true, validators: Validators.required}),
  });

  constructor(private userService: UserService) {}

  public Submit()
  {
    let body = {
      username: this.username?.value ?? "",
      password: this.password?.value ?? "",
      fullName: this.fullName?.value ?? "",
      email: this.email?.value ?? "",
    } as User;

    this.userService.InsertIndividual(body).subscribe(d => {
      alert(d.message);
    });
  }

  get username() { return this.formInsert.get('username'); }
  get password() { return this.formInsert.get('password'); }
  get fullName() { return this.formInsert.get('fullName'); }
  get email() { return this.formInsert.get('email'); }
}
