import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiResponse } from 'src/app/model/api-response';
import { User } from 'src/app/model/user.model';
import { UserService } from 'src/app/services/user.service';

interface FormUserUpdateModel
{
  username: FormControl<string>;
  password: FormControl<string>;
  fullName: FormControl<string>;
  email: FormControl<string>;
}

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {
  @Input() id!: string;
  public d!: User;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.fetchData();
  }

  public fetchData()
  {
    this.userService.GetIndividual(this.id).subscribe((d: ApiResponse<User>) => {
      console.log(d);
      this.d = d.data;
      let dl = d.data;
      this.username?.setValue(dl.username);
      this.email?.setValue(dl.email);
      this.fullName?.setValue(dl.fullName);
    });
  }

  public formField: string[] = ['username', 'fullName', 'email'];

  public formInsert: FormGroup<FormUserUpdateModel> = new FormGroup<FormUserUpdateModel>({
    username: new FormControl('', {nonNullable: true, validators: Validators.required}),
    password: new FormControl('', {nonNullable: true, validators: Validators.required}),
    fullName: new FormControl('', {nonNullable: true, validators: Validators.required}),
    email: new FormControl('', {nonNullable: true}),
  });

  public Submit()
  {
    let body = {
      username: this.username?.value ?? "",
      password: this.password?.value ?? "",
      fullName: this.fullName?.value ?? "",
      email: this.email?.value ?? "",
    } as User;

    this.userService.UpdateIndividual(this.id,body).subscribe(d => {
      alert(d.message);
      this.router.navigateByUrl("/master/users");
    });
  }

  get username() { return this.formInsert.get('username'); }
  get password() { return this.formInsert.get('password'); }
  get fullName() { return this.formInsert.get('fullName'); }
  get email() { return this.formInsert.get('email'); }
}
