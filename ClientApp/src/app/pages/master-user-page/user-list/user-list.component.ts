import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiResponse } from 'src/app/model/api-response';
import { User } from 'src/app/model/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  public data: User[] = [];

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.fetchData();
  }

  public fetchData()
  {
    this.userService.GetList()
      .subscribe((d: ApiResponse<User[]>) => {
        console.log(d);
        this.data = d.data;
      });
  }

  public Update(user: string)
  {
    this.router.navigateByUrl("/master/users/"+user);
  }

  public Delete(user: string)
  {
    let ask = confirm("Want to delete " + user + "?");
    if (ask)
    {
      this.userService.DeleteIndividual(user).subscribe(d => {
        alert(d.message);
        this.fetchData();
      });
    }
  }
}
