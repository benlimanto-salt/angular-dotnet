import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterProductPageComponent } from './master-product-page.component';
import { MasterProductPageRoutingModule } from './master-product-page-routing.module';
import { RouterModule } from '@angular/router';
import { ProductInsertComponent } from './product-insert/product-insert.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MasterProductPageRoutingModule,
    RouterModule,
    ReactiveFormsModule
  ],
  declarations: [MasterProductPageComponent, ProductInsertComponent, ProductListComponent]
})
export class MasterProductPageModule { }
