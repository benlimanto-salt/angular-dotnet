import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MasterProductPageComponent } from './master-product-page.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductInsertComponent } from './product-insert/product-insert.component';

const routes: Routes = [
  { 
    path: '', 
    component: MasterProductPageComponent ,
    children: [
      { path: '', component: ProductListComponent },
      { path: 'add', component: ProductInsertComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  declarations: []
})
export class MasterProductPageRoutingModule { }
