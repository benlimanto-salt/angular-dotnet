import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiResponse } from 'src/app/model/api-response';
import { Product } from 'src/app/model/product.model';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  public data: Product[] = [];

  constructor(private productService: ProductService, private router: Router) {}
  ngOnInit(): void {
    this.fetchData();
  }

  public fetchData()
  {
    this.productService.GetList().subscribe((d: ApiResponse<Product[]>) => {
      console.log(d);
      this.data = d.data;
    });
  }

  public Delete(id: number)
  {
    let ask = confirm("Do you want to delete?");
    if (ask)
    {
      this.productService.DeleteIndividual(id).subscribe(d => {
        alert(d.message);
        this.router.navigateByUrl("/master/product");
      });
    }
  }
}
