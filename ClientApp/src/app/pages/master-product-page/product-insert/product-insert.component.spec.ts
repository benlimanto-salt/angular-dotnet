import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductInsertComponent } from './product-insert.component';

describe('ProductInsertComponent', () => {
  let component: ProductInsertComponent;
  let fixture: ComponentFixture<ProductInsertComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductInsertComponent]
    });
    fixture = TestBed.createComponent(ProductInsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
