import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiResponse } from 'src/app/model/api-response';
import { Product } from 'src/app/model/product.model';
import { ProductService } from 'src/app/services/product.service';

interface FormInsertProductModel
{
  name: FormControl<string>;
  pricing: FormControl<number>;
  notes: FormControl<string>;
}

@Component({
  selector: 'app-product-insert',
  templateUrl: './product-insert.component.html',
  styleUrls: ['./product-insert.component.css']
})
export class ProductInsertComponent {

  public formField: any[] = [
    { name: "name", type: "text" },
    { name: "pricing", type: "number" },
    { name: "notes", type: "text" }
  ];

  public formInsert: FormGroup<FormInsertProductModel> = new FormGroup<FormInsertProductModel>({
    name: new FormControl('', {nonNullable: true }),
    pricing: new FormControl(0, {nonNullable: true }),
    notes: new FormControl('', {nonNullable: true }),
  });
  
  constructor(private productService: ProductService, private router: Router) {}
  
  public Submit()
  {
    let body = this.formInsert.value as Product;

    this.productService.InsertIndividual(body).subscribe((d: ApiResponse<any>) => {
      alert("Berhasil Insert");
      this.router.navigateByUrl("/master/product");
    });
  }


}
