import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthService } from "../services/auth.service";

@Injectable()
export class BearerHttpInterceptor implements HttpInterceptor {
  constructor(private authService:AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authService.GetToken();

    if (token != null) // If the app already authenticated
    {
      const newReq = req.clone({headers: req.headers.append("Authorization", "Bearer " + token)});
      console.log(newReq);
      return next.handle(newReq);
    }
    else
    {
      return next.handle(req);
    }
  }
}