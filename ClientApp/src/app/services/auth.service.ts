import { EventEmitter, Inject, Injectable, Output } from "@angular/core";
import { Service } from "./service";
import { HttpClient } from "@angular/common/http";
import { catchError, finalize, retry } from "rxjs";
import { ApiResponse } from "../model/api-response";

@Injectable({
    providedIn: 'root'
})
export class AuthService extends Service {

    @Output() loginLogoutEvent = new EventEmitter();

    constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string)
    {
        super();
    }

    Login(user?: string, pass?: string)
    {
        return this.http.post<ApiResponse<any>>(this.baseUrl + "auth/login", {
            username: user,
            password: pass
        })
        .pipe(retry(2),catchError(this.catchError), 
        finalize(() => {
            // Emit the event
            this.loginLogoutEvent.emit();
        }));
    }

    SetSession(token: string, expired: number)
    {
        localStorage.setItem("jwt_token", token);
        localStorage.setItem("jwt_expired", expired.toString());
    }

    GetToken()
    {
        return localStorage.getItem("jwt_token");
    }

    IsExpired()
    {
        let epoch = parseInt(localStorage.getItem("jwt_expired") ?? "");
        
        return epoch - Date.now() <= 0;
    }

    IsLoggedIn()
    {
        if (this.GetToken() == null || this.IsExpired())
        {
            return false;
        }
        
        return true;
    }

    LogOut()
    {
        localStorage.clear();
        this.loginLogoutEvent.emit();
    }
}
