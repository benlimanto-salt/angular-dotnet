import { Injectable } from '@angular/core';
import { Service } from './service';
import { HttpClient } from '@angular/common/http';
import { ApiResponse } from '../model/api-response';
import { Product } from '../model/product.model';
import { environment } from 'src/environments/environment';
import { catchError, retry } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends Service {

  constructor(private http: HttpClient) {
    super()
  }

  public GetList()
  {
    return this.http.get<ApiResponse<Product[]>>(environment.BASE_URL + "api/product")
      .pipe(
        retry(2),
        catchError(this.catchError)
      );
  }

  public GetIndividual(id: number)
  {
    return this.http.get<ApiResponse<Product>>(environment.BASE_URL + "api/product/" + id.toString())
      .pipe(
        retry(2),
        catchError(this.catchError)
      );
  }
  
  public DeleteIndividual(id: number)
  {
    return this.http.delete<ApiResponse<any>>(environment.BASE_URL + "api/product/" + id.toString())
      .pipe(
        retry(2),
        catchError(this.catchError)
      );
  }

  public InsertIndividual(body: Product)
  {
    return this.http.post<ApiResponse<any>>(environment.BASE_URL + "api/product", body)
      .pipe(
        retry(2),
        catchError(this.catchError)
      );
  }

  public UpdateIndividual(id: number, body: Product)
  {
    return this.http.post<ApiResponse<any>>(environment.BASE_URL + "api/product/" + id.toString(), body)
      .pipe(
        retry(2),
        catchError(this.catchError)
      );
  }

}
