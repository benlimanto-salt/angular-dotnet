import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { Observable, throwError } from "rxjs";

export abstract class Service {

    catchError(err: any, caught:Observable<any>) 
    {
        console.log(err);
        console.log(caught);
                
        return throwError(() => new Error('Something bad happened; please try again later.'));;
    }
}