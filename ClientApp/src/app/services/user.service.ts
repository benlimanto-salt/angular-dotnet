import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Service } from './service';
import { catchError, retry } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiResponse } from '../model/api-response';
import { User } from 'oidc-client';

@Injectable({
  providedIn: 'root'
})
export class UserService extends Service {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    super();
  }

  public GetTask()
  {
    return this.http.get(environment.BASE_URL + "api/tasks")
      .pipe(
        retry(2), 
        catchError(this.catchError)
      );
  }

  public GetList()
  {
    return this.http.get<ApiResponse<User[]>>(environment.BASE_URL + "api/users")
      .pipe(
        retry(2), 
        catchError(this.catchError)
      );
  }
  
  public GetIndividual(username: string)
  {
    return this.http.get<ApiResponse<User>>(environment.BASE_URL + "api/users/" + username)
      .pipe(
        retry(2), 
        catchError(this.catchError)
      );
  }
  
  public DeleteIndividual(username: string)
  {
    return this.http.delete<ApiResponse<any>>(environment.BASE_URL + "api/users/" + username)
      .pipe(
        retry(2), 
        catchError(this.catchError)
      );
  }

  public InsertIndividual(body: any)
  {
    return this.http.post<ApiResponse<any>>(environment.BASE_URL + "api/users", body)
      .pipe(
        retry(2), 
        catchError(this.catchError)
      );
  }

  public UpdateIndividual(username: string, body: any)
  {
    return this.http.patch<ApiResponse<any>>(environment.BASE_URL + "api/users/" + username, body)
      .pipe(
        retry(2), 
        catchError(this.catchError)
      );
  }

}
