using System.Collections.Generic;
using angular_net.Models;
using Microsoft.EntityFrameworkCore;

namespace angular_net.Database
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base (options) 
        {
        }

        public DbSet<User> User {get;set;} = null!;

        public DbSet<Product> Product {get;set;} = null!;
    
    }
}