﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace angular_net.Migrations
{
    /// <inheritdoc />
    public partial class RenameProductTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "Product",
                newName: "product");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "product",
                newName: "Product");
        }
    }
}
