using angular_net.Authentication.Middleware;
using Microsoft.AspNetCore.Authorization;

namespace angular_net.Authentication.Roles;

public static class RolesPool
{
    public static IServiceCollection AddAuthorizationRoles(this IServiceCollection services)
    {
        services.AddAuthorization(o => {
            o.AddPolicy("RequireAdminRole", p => p.RequireRole("admin"));
        });

        services.AddAuthorization(o => {
            o.AddPolicy("ProductRole", p => p.RequireRole("warehouse","admin"));
        });

        services.AddScoped<IAuthorizationMiddlewareResultHandler, AuthorizationOverrideMiddleware>();
        
        return services;
    }
}