using System.IdentityModel.Tokens.Jwt;
using angular_net.Models.Responses;
using angular_net.Repositories.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Policy;

namespace angular_net.Authentication.Middleware;

public class AuthorizationOverrideMiddleware : IAuthorizationMiddlewareResultHandler
{
    private IUserRepository _userRepo;
    public AuthorizationOverrideMiddleware(IUserRepository userRepository)
    {
        _userRepo = userRepository;
    }

    private readonly AuthorizationMiddlewareResultHandler defaultHandler = new();

    public async Task HandleAsync(
        RequestDelegate next, 
        HttpContext context, 
        AuthorizationPolicy policy, 
        PolicyAuthorizationResult authorizeResult)
    {

        // var usr = context.User.Claims
        var usr = "";
        if (context.User.Claims.Count() > 0)
        {
            usr = context.User.Claims
                .Where(q => q.Type.Contains("nameidentifier"))
                .First()?.Value ?? "";
        }
        
        if(usr != "")
        {
            var usrObj = await _userRepo.GetUser(usr);
            if (usrObj == null)
            {
                // Return a 404 to make it appear as if the resource doesn't exist.
                context.Response.StatusCode = StatusCodes.Status404NotFound;
                await context.Response.WriteAsJsonAsync(new ApiResponse() 
                {
                    Status = 404,
                    Message = "Who are you? No Such User Found!"
                });
                return;
            }
        }

        // Fall back to the default implementation.
        await defaultHandler.HandleAsync(next, context, policy, authorizeResult);
    }
}