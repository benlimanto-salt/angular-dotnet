using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace angular_net.Models;

[Table("product")]
public class Product
{
    [JsonPropertyName("id")]
    public int Id { get; set; }

    [JsonPropertyName("name")]
    [Required]
    public string Name { get; set; } = String.Empty;
    
    [JsonPropertyName("pricing")]
    [Required]
    public int Pricing { get; set; }
    
    [JsonPropertyName("notes")]
    [Required]
    public string Notes { get; set; } = String.Empty;
}