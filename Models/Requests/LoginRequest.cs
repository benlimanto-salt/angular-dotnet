using System.Text.Json.Serialization;

namespace angular_net.Models.Requests;

public class LoginRequest
{
    [JsonPropertyName("username")]
    public string Username {get; set;} = null!;

    [JsonPropertyName("password")]
    public string Password {get; set;} = null!;

}