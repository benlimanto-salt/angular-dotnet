using System.Text.Json.Serialization;

namespace angular_net.Models.Requests;

public class UserRequest : User
{
    [JsonPropertyName("password")]
    public new string Password { get; set; } = String.Empty;
}