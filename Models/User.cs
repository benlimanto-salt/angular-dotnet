using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace angular_net.Models;

[Table("user")]
public class User
{
    public int Id { get; set; }

    [JsonPropertyName("username")]
    public string Username { get; set; } = String.Empty;
    [JsonIgnore]
    public string Password { get; set; } = String.Empty;
    [JsonPropertyName("fullName")]
    public string FullName { get; set; } = String.Empty;
    [JsonPropertyName("email")]
    public string Email { get; set; } = String.Empty;
}